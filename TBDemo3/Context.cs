﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBDemo3
{
    public class Context
    {
        public string Message { get; set; }
        public Context()
        {
            Message = "Autre message";
        }
    }
}
