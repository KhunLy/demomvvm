﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace TBDemo3
{
    //public class test
    //{
    //    public event Action monEvenement
    //    {
    //        add {
    //            // autre chose
    //            monEvenement += value; 
    //        } 
    //        remove { monEvenement -= value; } 
    //    }
    //}

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private string message;
        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
                RaisePropertyChanged();
            }
        }

        private int _size;

        public int Size
        {
            get { return _size; }
            set { _size = value;RaisePropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<User> Collection { get; set; }

        public MainWindow()
        {
            Message = "Hello World";
            Size = 100;
            Collection = new ObservableCollection<User>
            {
                new User { Name = "Mike", Age = 38, BirthDate = new DateTime(1982,3,17), IsYellow = false },
                new User { Name = "Khun", Age = 38, BirthDate = new DateTime(1982,5,6), IsYellow = true },
                new User { Name = "Piv", Age = 39, BirthDate = new DateTime(1981,4,30), IsYellow = true },
            };
            DataContext = this;
            InitializeComponent();
            //Button btn = new Button();
            //btn.Content = "Click Me !!";
            //Content = btn;
            //btn.Click += OnClick;
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {

            Message = "Le message a changé";
            // créer une nouvelle instance d'une fenetre
            //Page2 page = new Page2();
            ////afficher la fenetre
            //page.Show();
            //fermer une fenetre
            //this.Close();
            //boite de dialog classique
            //MessageBoxResult result =
            //    MessageBox.Show("Hello World!!", "Confirmation", MessageBoxButton.YesNoCancel);
            //if(result == MessageBoxResult.Yes)
            //{
            //    // do something
            //}
            //else if(result == MessageBoxResult.No)
            //{
            //    //do something else
            //}
            //else
            //{
            //    // do something else
            //}

            // ouverture file
            //OpenFileDialog dlg = new OpenFileDialog();
            //dlg.ShowDialog();

            // sauer fichier
            //SaveFileDialog slg = new SaveFileDialog();
            //slg.ShowDialog();

            // imprimer un ficher
            //PrintDialog pdg = new PrintDialog();
            //pdg.ShowDialog();
        }

        private void RaisePropertyChanged([CallerMemberName]string propName = "")
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        private void Increase(object sender, RoutedEventArgs e)
        {
            Size++;
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            Collection.Add(new User
            {
                Name = "Steve",
                IsYellow = false,
                Age = 32,
                BirthDate = new DateTime(1988, 1, 1)
            });
        }
    }
}
