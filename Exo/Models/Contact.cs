﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo.Models
{
    class Contact
    {
        public string Nom { get; set; }
        public string Tel { get; set; }
    }
}
