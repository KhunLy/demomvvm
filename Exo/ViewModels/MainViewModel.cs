﻿using Exo.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;

namespace Exo.ViewModels
{
    class MainViewModel : BindableBase
    {
		private string _nom;

		public string Nom
		{
			get { return _nom; }
			set { _nom = value; RaisePropertyChanged(); }
		}

		private string _tel;

		public string Tel
		{
			get { return _tel; }
			set { _tel = value; RaisePropertyChanged(); }
		}

		private ObservableCollection<Contact> _contacts;

		public ObservableCollection<Contact> Contacts
		{
			get { return _contacts; }
			set { _contacts = value; RaisePropertyChanged(); }
		}

		public RelayCommand AddCommand { get; set; }

		public RelayCommand<Contact> DeleteCommand { get; set; }

		public MainViewModel()
		{
			Contacts = new ObservableCollection<Contact>();
			AddCommand = new RelayCommand(Ajouter);
			DeleteCommand = new RelayCommand<Contact>(Supprimer);
		}

		public void Ajouter()
		{
			Contact c = new Contact();
			c.Nom = Nom;
			c.Tel = Tel;
			Contacts.Add(c);
			Nom = null;
			Tel = null;
		}

		public void Supprimer(Contact c)
		{
			Contacts.Remove(c);
		}



	}
}
