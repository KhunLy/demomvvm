﻿using Newtonsoft.Json;
using Pokedex.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex.Services
{
    class PokemonService
    {
        public IndexPokemon GetIndex(string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(uri).Result;
                if(response.IsSuccessStatusCode)
                {
                    HttpContent content = response.Content;
                    string json = content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<IndexPokemon>(json);
                }
                return null;
            }
        }
    }
}
