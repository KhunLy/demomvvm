﻿using Pokedex.Models;
using Pokedex.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.MVVM.BaseClasses;

namespace Pokedex.ViewModels
{
    class ListViewModel : BindableBase
    {
		private ObservableCollection<SimplePokemon> _pokemons;

		public ObservableCollection<SimplePokemon> Pokemons
		{
			get { return _pokemons; }
			set { _pokemons = value; }
		}

		public ListViewModel()
		{
			PokemonService pService = new PokemonService();
			IndexPokemon index =
				pService.GetIndex("https://pokeapi.co/api/v2/pokemon");
			Pokemons = new ObservableCollection<SimplePokemon>(index.Results);
		}

	}
}
