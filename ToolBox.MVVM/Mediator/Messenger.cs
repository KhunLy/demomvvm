﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.MVVM.Mediator
{
    public class Messenger
    {
        #region Singleton (anti)Pattern
        private static Messenger _instance;
        public static Messenger Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Messenger();
                return _instance;
            }
        }
        private Messenger()
        {
            _events = new Dictionary<string, Action>();
        }
        #endregion

        private Dictionary<string, Action> _events;

        public void Subsribe() { }
        public void Publish() { }
    }
}
